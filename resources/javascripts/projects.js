function getGithubRepos() {
  return $.getJSON("https://api.github.com/users/jjghali/repos");
}

function getGitlabRepos() {
  return $.getJSON("https://gitlab.com/api/v4/users/jjghali/projects");
}

var requests = [getGithubRepos(), getGitlabRepos()];

$.when.apply($, requests).done(function() {
  let githubRepos = arguments[0][2].responseJSON;
  let gitlabRepos = arguments[1][2].responseJSON;

  console.log(gitlabRepos);
  gitlabRepos.forEach(gh => {
    $(document).ready(function() {
      $(".projects-partial").append(
        '<div class="col-sm-6 col-md-12 col-lg-6 col-xl-4 mb-3 project-card"><div class="gitlab-component height-full text-left box-shadow bg-white rounded-1 p-3"><div class="d-flex flex-justify-between flex-items-start mb-1"><h1 class="f4 lh-condensed mb-1"><a href="' +
          gh.web_url +
          '"><i class="fab fa-gitlab tint-gitlab provider-icon"></i><span class="text-normal">jjghali /</span>' +
          gh.name +
          '</a></h1></div><div class="text-gray mb-2 ws-normal">' +
          (gh.description != null ? gh.description : "") +
          "</div></div></div>"
      );
    });
  });
  githubRepos.forEach(gh => {
    $(document).ready(function() {
      $(".projects-partial").append(
        '<div class="col-sm-6 col-md-12 col-lg-6 col-xl-4 mb-3 project-card"><div class="github-component height-full text-left box-shadow bg-white rounded-1 p-3"><div class="d-flex flex-justify-between flex-items-start mb-1"><h1 class="f4 lh-condensed mb-1"><a href="' +
          gh.html_url +
          '"><i class="fab fa-github tint-github provider-icon"></i><span class="text-normal">jjghali /</span>' +
          gh.name +
          '</a></h1></div><div class="text-gray mb-2 ws-normal">' +
          (gh.description != null ? gh.description : "") +
          "</div></div></div>"
      );
    });
  });
});
